import 'package:flutter/material.dart';
import 'package:flutter_skeleton/constants/routes_constants.dart' as routes;
import 'package:flutter_skeleton/ui/views/home_view.dart';
import 'package:flutter_skeleton/ui/views/login_view.dart';

class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {

    switch(settings.name) {
      case routes.home:
        return MaterialPageRoute(builder: (_) => HomeView());
      case routes.login:
        return MaterialPageRoute(builder: (_) => LoginView());
      default:
        return MaterialPageRoute(builder: (_) {
          return Scaffold(
            body: Center(
              child: Text("no route defined for ${settings.name}"),
            ),
          );
        });
    }
  }
}