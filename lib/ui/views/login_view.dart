import 'package:flutter/material.dart';
import 'package:flutter_skeleton/core/viewmodels/login_model.dart';
import 'package:flutter_skeleton/ui/views/base_view.dart';

class LoginView extends StatefulWidget {
  @override
  _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  @override
  Widget build(BuildContext context) {
    return BaseView<LoginModel>(
      builder: (context, model, child) => Scaffold(
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Center(
              child: RaisedButton(
                onPressed: () {
                  model.login();
                },
                child: Text("Log in"),
                ),
            )
          ],
        ),
      ),
    );
  }
}