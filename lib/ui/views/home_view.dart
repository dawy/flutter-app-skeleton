import 'package:flutter/material.dart';
import 'package:flutter_skeleton/ui/views/base_view.dart';
import 'package:flutter_skeleton/core/viewmodels/home_model.dart';

class HomeView extends StatefulWidget {
  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  @override
  Widget build(BuildContext context) {
    return BaseView<HomeModel>(
      builder: (context, model, child) => Scaffold(
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Center(
              child: RaisedButton(
                onPressed: () {
                  model.logout();
                },
                child: Text("Logout"),
              ),
            ),
          ],
        ),
      )
    );
  }
}