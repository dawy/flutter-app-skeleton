import 'package:flutter_skeleton/core/services/navigation_service.dart';
import 'package:flutter_skeleton/core/viewmodels/base_model.dart';
import 'package:flutter_skeleton/locator.dart';
import 'package:flutter_skeleton/constants/routes_constants.dart' as routes;


class HomeModel extends BaseModel {
  NavigationService _navigationService = locator<NavigationService>();

  Future logout() {
    _navigationService.goBack();
  }

}