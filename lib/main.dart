import 'package:flutter/material.dart';
import 'package:flutter_skeleton/constants/routes_constants.dart' as routes;
import 'package:flutter_skeleton/core/services/navigation_service.dart';
import 'package:flutter_skeleton/ui/router.dart';
import 'package:flutter_skeleton/locator.dart';

void main() {
  setupLocator();
  runApp(MyApp());
}  

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter skeleton',
      navigatorKey: locator<NavigationService>().navigatorKey,
      initialRoute: routes.login,
      onGenerateRoute: Router.generateRoute,
    );
  }
}