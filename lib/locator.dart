import 'package:flutter_skeleton/core/services/navigation_service.dart';
import 'package:flutter_skeleton/core/viewmodels/base_model.dart';
import 'package:flutter_skeleton/core/viewmodels/home_model.dart';
import 'package:flutter_skeleton/core/viewmodels/login_model.dart';
import 'package:get_it/get_it.dart';

GetIt locator = GetIt.instance;

void setupLocator() {
  locator.registerFactory(() => LoginModel());
  locator.registerFactory(() => HomeModel());
  locator.registerLazySingleton(() => NavigationService());
}